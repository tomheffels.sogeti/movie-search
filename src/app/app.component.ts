import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-search-bar></app-search-bar>
    <div class="navbar-margin"></div>
    <div class="navbar-margin d-sm-none"></div>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {}

export interface ApiResponse {
  Response: string;
  Search: any[];
  totalResults: string;
}

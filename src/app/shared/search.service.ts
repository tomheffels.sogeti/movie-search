import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(
    private http: HttpClient,
  ) { }

  searchByName(query: string): Observable<Object>  {
    return this.http.get(`http://www.omdbapi.com/?s=${query}&apiKey=7ed78cce`)
  }

  searchById(movieId: string, plotLength: string): Observable<Object> {
    return this.http.get(`http://www.omdbapi.com/?i=${movieId}&plot=${plotLength}&apikey=7ed78cce`)
  }
}

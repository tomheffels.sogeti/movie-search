import { SearchService } from '../shared/search.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiResponse } from '../shared/api-response.model';

@Component({
  selector: 'app-search-results',
  template: `
    <div class="container mx-auto w-100">
      <h4 class="page-title">Search results for: {{query}}</h4>
      <app-search-result *ngFor="let movie of movies" [movieId]="movie.imdbID" [plotLength]="plotLength" ></app-search-result>
    </div>
  `
})
export class SearchResultsComponent implements OnInit {
  query: string
  plotLength: string
  movies: any[]

  constructor(
    private searchService: SearchService,
    private route: ActivatedRoute,
    private router: Router) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

  ngOnInit(): void {
    this.query = this.route.snapshot.paramMap.get("query");
    this.plotLength = this.route.snapshot.paramMap.get("plotLength");
    this.searchService.searchByName(this.query)
      .subscribe(
        (results: ApiResponse) => this.movies = results.Search.slice(0,5),
        err => console.error(err)
      )
  }
}

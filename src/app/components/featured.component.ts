import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-featured',
  template: `
    <div class="container mx-auto w-100">
      <h4 class="page-title">Featured Movies:</h4>
      <app-search-result movieId="tt0208092" plotLength="full"></app-search-result>
      <app-search-result movieId="tt0118715" plotLength="full"></app-search-result>
    </div>
  `
})
export class FeaturedComponent {}

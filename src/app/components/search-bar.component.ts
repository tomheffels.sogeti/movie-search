import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styles: [`
  .navbar {background-color: #9A998E; margin-bottom}
  .navbar-brand {font-size: 28px; color: #F5F1E3}
  .btn {background-color: #F5F1E3}`]
})
export class SearchBarComponent {
  query: string = ""
  plotLength: string = "full"

  constructor(private router: Router) { }

  onSearch() {
    const { query, plotLength } = this
    this.router.navigate(['search', query, plotLength])
  }
}

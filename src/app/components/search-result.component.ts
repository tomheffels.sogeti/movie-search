import { SearchService } from '../shared/search.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styles: [`
    ul, li {list-style: none; margin: 0; padding-left: 8px;}
    .genre {margin-left: -16px}
    .movie-plot {cursor: pointer;}
  `]
})
export class SearchResultComponent implements OnInit {
  @Input("movieId") movieId: string
  @Input("plotLength") plotLength: string
  movie: any
  fullPlot: boolean = false

  constructor(
    private SearchService: SearchService,
  ) { }

  ngOnInit(): void {
    this.SearchService.searchById(this.movieId, this.plotLength)       
      .subscribe(
        movie => this.movie = movie,
        err => console.error(err)
      )
  }

  togglePlot() {
    this.fullPlot = !this.fullPlot
  }
}
